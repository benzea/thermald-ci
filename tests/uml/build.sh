#!/bin/sh
set -e

SOURCE_DIR=$( realpath $( dirname "$0" ) )

wget https://cdn.kernel.org/pub/linux/kernel/v5.x/linux-5.9.10.tar.xz
tar -xf linux-5.9.10.tar.xz 

cd linux-5.9.10
cp "$SOURCE_DIR/kernel-config.uml" .config

export ARCH=um

# Select defaults for everything else
yes "" | make oldconfig
make linux
cp linux ../
cd ..
